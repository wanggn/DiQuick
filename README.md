<p>DiQuick Web UI 框架，包含诸多组件：Nav、Tab、Media、Form、Menu、Slider、Dialog 等，简化的HTML结构与预定义CSS，可快速创建响应式布局，在线自定义风格工具轻松创建私有样式，支持外部加载的组件自动绑定事件，致力于开发轻量化、语义化、扩展性强的 Web UI 框架。</p>

<p>软件中文首页<br />
<a href="http://www.diquick.com/" target="_blank">http://www.diquick.com/</a></p>

<p><img alt="" src="https://gitee.com/uploads/images/2019/0409/184932_0b671483_1073834.jpeg" /></p>